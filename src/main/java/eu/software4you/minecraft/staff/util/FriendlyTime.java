package eu.software4you.minecraft.staff.util;

import eu.software4you.configuration.SimpleConfigurationWrapper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class FriendlyTime {
    private static SimpleConfigurationWrapper layout;

    public static void setLayout(SimpleConfigurationWrapper layout) {
        FriendlyTime.layout = layout;
    }

    public static String time(long timestamp) {
        SimpleConfigurationWrapper layout = FriendlyTime.layout.sub("time");
        if (timestamp < 0)
            return layout.string("never", (Object) "Never");
        return new SimpleDateFormat(layout.string("format", (Object) "MM/dd/YYYY hh:mm a")).format(new Date(timestamp));
    }

    public static String duration(long millis) {
        SimpleConfigurationWrapper layout = FriendlyTime.layout.sub("duration");

        if (millis < 0)
            return layout.string("permanent", (Object) "Permanent");

        StringBuilder s = new StringBuilder();
        long days = TimeUnit.MILLISECONDS.toDays(millis);
        long years = days / 365;
        if (years > 0) {
            s.append(years);
            s.append(String.format("%s ", layout.string("year", (Object) "y")));
        }
        days -= years * 365;
        long months = days / 30;
        if (months > 0) {
            s.append(months);
            s.append(String.format("%s ", layout.string("month", (Object) "mth")));
        }
        days -= months * 30;
        if (days > 0) {
            s.append(days);
            s.append(String.format("%s ", layout.string("day", (Object) "d")));
        }
        millis -= TimeUnit.DAYS.toMillis(days);
        long hours = TimeUnit.MILLISECONDS.toHours(millis);
        if (hours > 0) {
            s.append(hours);
            s.append(String.format("%s ", layout.string("hour", (Object) "h")));
        }
        millis -= TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        if (minutes > 0) {
            s.append(minutes);
            s.append(String.format("%s ", layout.string("minute", (Object) "m")));
        }
        millis -= TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);
        if (seconds > 0) {
            s.append(seconds);
            s.append(String.format("%s ", layout.string("second", (Object) "s")));
        }
        return s.toString().trim();
    }
}
