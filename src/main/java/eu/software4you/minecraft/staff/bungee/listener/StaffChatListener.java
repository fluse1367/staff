package eu.software4you.minecraft.staff.bungee.listener;

import eu.software4you.minecraft.staff.bungee.Main;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class StaffChatListener implements Listener {
    private final String prefix;
    private final String adminPrefix;

    public StaffChatListener() {
        prefix = Main.getInstance().getConfig().string("chat-prefix.normal");
        adminPrefix = Main.getInstance().getConfig().string("chat-prefix.admin");
    }

    @EventHandler
    public void handle(ChatEvent e) {
        ProxiedPlayer p = (ProxiedPlayer) e.getSender();
        if (e.getMessage().startsWith(prefix) && p.hasPermission(Main.getInstance().getConfig().string("permission.chat.send"))) {
            e.setCancelled(true);
            if (optedOut(p))
                return;
            Main.getInstance().queueBroadcastStaffChat(p, e.getMessage().substring(prefix.length()));
        } else if (e.getMessage().startsWith(adminPrefix) && p.hasPermission(Main.getInstance().getConfig().string("permission.admin-chat.send"))) {
            e.setCancelled(true);
            if (optedOut(p))
                return;
            Main.getInstance().queueBroadcastAdminChat(p, e.getMessage().substring(adminPrefix.length()));
        }
    }

    private boolean optedOut(ProxiedPlayer p) {
        if (!Main.getInstance().isReceivingStaffNotifications(p.getUniqueId())) {
            p.sendMessage(new TextComponent(Main.getInstance().getLayout().string("error.opted-out")));
            return true;
        }
        return false;
    }
}
