package eu.software4you.minecraft.staff.bungee.punishment;

import eu.software4you.minecraft.staff.bungee.Main;
import eu.software4you.sql.SqlEngine;
import eu.software4you.utils.BungeeUtils;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.scheduler.ScheduledTask;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class PunishmentHandler implements Listener {
    private final SqlEngine engine;
    private final ScheduledTask garbageTask;
    private final Map<UUID, Map<Punishment.Type, PlayerPunishment>> activePunishments = new HashMap<>();

    public PunishmentHandler(SqlEngine engine) {
        this.engine = engine;
        garbageTask = Main.getInstance().async(() -> {
            activePunishments.keySet().removeIf(uuid ->
                    Main.getInstance().getProxy().getPlayer(uuid) == null);
            activePunishments.forEach((uuid, map) ->
                    map.entrySet().removeIf(en -> !en.getValue().isActive()));
        }, 1, 1, TimeUnit.MINUTES);
    }

    private boolean areActivePunishmentsLoaded(UUID player) {
        return activePunishments.containsKey(player);
    }

    private void loadActivePunishments(UUID player) {
        if (areActivePunishmentsLoaded(player))
            throw new IllegalStateException("Player already loaded");
        try {
            ResultSet res = engine.query(String.format(
                    "select * from player_punishments where player = '%s' and (expires > %d or expires < 0)",
                    player.toString(), System.currentTimeMillis()));
            Map<Punishment.Type, PlayerPunishment> map = new HashMap<>();
            while (res.next()) {
                Punishment punishment = Main.getInstance().getPunishmentManager().getPunishment(res.getInt("punishment"));
                map.put(punishment.getType(), PlayerPunishment.fromResultSet(engine, this, res));
            }
            activePunishments.put(player, map);

        } catch (SQLException e) {
            throw new Error(e);
        }
    }

    private void unloadActivePunishments(UUID player) {
        if (!areActivePunishmentsLoaded(player))
            throw new IllegalStateException("Player not loaded");
        activePunishments.remove(player);
    }

    private Map<Punishment.Type, PlayerPunishment> getActivePunishmentsRaw(UUID player) {
        if (!areActivePunishmentsLoaded(player))
            loadActivePunishments(player);
        Map<Punishment.Type, PlayerPunishment> map = activePunishments.get(player);
        map.entrySet().removeIf(en -> !en.getValue().isActive());
        return map;
    }

    public Collection<PlayerPunishment> getActivePunishments(UUID player) {
        return Collections.unmodifiableCollection(getActivePunishmentsRaw(player).values());
    }

    public Collection<PlayerPunishment> collectFormerPunishments(UUID player) {
        try {
            ResultSet res = engine.query(String.format(
                    "select * from player_punishments where player = '%s' and expires < %d and expires > 0 order by timestamp",
                    player.toString(), System.currentTimeMillis()));
            List<PlayerPunishment> list = new ArrayList<>();
            while (res.next()) {
                list.add(PlayerPunishment.fromResultSet(engine, this, res));
            }
            return Collections.unmodifiableList(list);
        } catch (SQLException e) {
            throw new Error(e);
        }
    }

    public PlayerPunishment getActivePunish(UUID player, Punishment.Type type) {
        return getActivePunishmentsRaw(player).get(type);
    }

    public boolean hasActivePunish(UUID player, Punishment.Type type) {
        return getActivePunish(player, type) != null;
    }

    boolean applyPunishment(PlayerPunishment punishment) {
        if (!punishment.isActive())
            return false;
        if (hasActivePunish(punishment.getPlayer(), punishment.getPunishment().getType()))
            return false;
        getActivePunishmentsRaw(punishment.getPlayer()).put(punishment.getPunishment().getType(), punishment);
        punishActionPlayer(punishment);
        return true;
    }

    void punishActionPlayer(PlayerPunishment punishment) {
        ProxiedPlayer t = Main.getInstance().getProxy().getPlayer(punishment.getPlayer());
        if (t != null) {
            switch (punishment.getPunishment().getType()) {
                case BAN:
                    t.disconnect(new TextComponent(getMessage(punishment)));
                    break;
                case MUTE:
                    t.sendMessage(new TextComponent(getMessage(punishment)));
                    break;
            }
        }
    }

    void deletePunishment(PlayerPunishment punishment) {
        if (areActivePunishmentsLoaded(punishment.getPlayer()))
            getActivePunishmentsRaw(punishment.getPlayer()).remove(punishment.getPunishment().getType());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void handle(LoginEvent e) {
        UUID player = e.getConnection().getUniqueId();
        if (!areActivePunishmentsLoaded(player))
            loadActivePunishments(player);
        PlayerPunishment punishment = getActivePunish(player, Punishment.Type.BAN);
        if (punishment == null || !punishment.isActive())
            return;
        e.setCancelled(true);
        e.setCancelReason(new TextComponent(getMessage(punishment)));
        unloadActivePunishments(player);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void handle(PlayerDisconnectEvent e) {
        UUID player = e.getPlayer().getUniqueId();
        if (areActivePunishmentsLoaded(player))
            unloadActivePunishments(player);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void handle(ChatEvent e) {
        if(e.getMessage().startsWith("/"))
            return;
        ProxiedPlayer p = ((ProxiedPlayer) e.getSender());
        PlayerPunishment punishment = getActivePunish(p.getUniqueId(), Punishment.Type.MUTE);
        if (punishment == null || !punishment.isActive())
            return;
        e.setCancelled(true);
        p.sendMessage(new TextComponent(getMessage(punishment)));
    }

    private String getMessage(PlayerPunishment punishment) {
        return Main.getInstance().getLayout().string("punishments." + punishment.getPunishment().getType().name().toLowerCase(),
                punishment.getId().toString(),
                String.valueOf(punishment.getPunishment().getId()),
                punishment.getPunishment().getName(),
                punishment.getExpiresFriendly(),
                BungeeUtils.getMainUserCache().getUsername(punishment.getPunisher())
        );
    }
}
