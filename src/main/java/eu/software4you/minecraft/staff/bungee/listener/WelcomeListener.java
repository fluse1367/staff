package eu.software4you.minecraft.staff.bungee.listener;

import eu.software4you.minecraft.staff.bungee.Main;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class WelcomeListener implements Listener {
    @EventHandler
    public void handle(PostLoginEvent e) {
        ProxiedPlayer p = e.getPlayer();
        if (!p.hasPermission(Main.getInstance().getConfig().string("permission.chat.send")))
            return;
        p.sendMessage(new TextComponent(Main.getInstance().getLayout().string("welcome."
                + (Main.getInstance().isReceivingStaffNotifications(p.getUniqueId()) ? "enabled" : "disabled"))));
    }
}
