package eu.software4you.minecraft.staff.bungee.command;

import eu.software4you.minecraft.staff.bungee.Main;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class ToggleCommand extends Command {
    private final Main main;
    public ToggleCommand() {
        super("togglenotifications", Main.getInstance().getConfig().string("permission.toggle-notifications"), "tn");
        main = Main.getInstance();
    }

    @Override
    public void execute(CommandSender s, String[] a) {
        if (!(s instanceof ProxiedPlayer))
            return;
        main.async(() -> {
            ProxiedPlayer p = (ProxiedPlayer) s;
            if (main.isReceivingStaffNotifications(p.getUniqueId())) {
                main.setReceivingStaffNotifications(p.getUniqueId(), false);
                p.sendMessage(new TextComponent(main.getLayout().string("toggle.disabled.self", p.getDisplayName())));
                main.broadcastStaffChat(main.getLayout().string("toggle.disabled.other", p.getDisplayName()));
            } else {
                main.broadcastStaffChat(main.getLayout().string("toggle.enabled.other", p.getDisplayName()));
                main.setReceivingStaffNotifications(p.getUniqueId(), true);
                p.sendMessage(new TextComponent(main.getLayout().string("toggle.enabled.self", p.getDisplayName())));
            }
        });
    }
}
