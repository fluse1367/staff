package eu.software4you.minecraft.staff.bungee.command;

import eu.software4you.minecraft.staff.bungee.Main;
import eu.software4you.minecraft.staff.bungee.punishment.Punishment;
import eu.software4you.minecraft.staff.bungee.report.ReportReason;
import eu.software4you.minecraft.staff.util.NumberUtil;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class StaffAdminCommand extends Command {
    public StaffAdminCommand() {
        super("staffadmin", Main.getInstance().getConfig().string("permission.admin"));
    }

    @Override
    public void execute(CommandSender s, String[] args) {
        if (args.length > 0) {
            switch (args[0].toLowerCase()) {
                case "reportreasons":
                    if (args.length > 1) {
                        switch (args[1].toLowerCase()) {
                            case "list":
                                ReportCommand.listReasons(s);
                                return;
                            case "add":
                                if (args.length > 3) {
                                    String sid = args[2];
                                    if (!NumberUtil.acceptableAsInt(sid)) {
                                        Main.getInstance().getLayout().sendString(s, "error.not-a-number", sid);
                                        return;
                                    }
                                    int id = Integer.parseInt(sid);
                                    if (Main.getInstance().getReportManager().isReason(id)) {
                                        Main.getInstance().getLayout().sendString(s, "admin.reportreasons.add.already", sid);
                                        return;
                                    }

                                    String name = ChatColor.translateAlternateColorCodes('&', args[3].replace("_", " "));

                                    ReportReason reason = new ReportReason(id, name);
                                    Main.getInstance().getReportManager().addReason(reason);
                                    Main.getInstance().getLayout().sendString(s, "admin.reportreasons.add", sid, reason.getName());
                                    return;
                                }
                                break;
                            case "del":
                                if (args.length > 2) {
                                    String sid = args[2];
                                    if (!NumberUtil.acceptableAsInt(sid)) {
                                        Main.getInstance().getLayout().sendString(s, "error.not-a-number", sid);
                                        return;
                                    }
                                    int id = Integer.parseInt(sid);
                                    ReportReason reason = Main.getInstance().getReportManager().getReason(id);
                                    if (reason == null) {
                                        Main.getInstance().getLayout().sendString(s, "report.reasons.error.not-found", sid);
                                        return;
                                    }
                                    Main.getInstance().getReportManager().removeReason(reason);
                                    Main.getInstance().getLayout().sendString(s, "admin.reportreasons.del", sid, reason.getName());
                                    return;
                                }
                                break;
                        }
                    }
                    break;
                case "punishments":
                    if (args.length > 1) {
                        switch (args[1].toLowerCase()) {
                            case "list":
                                PunishCommand.listPunishments(s);
                                return;
                            case "add":
                                if (args.length > 5) {
                                    String pid = args[2];
                                    if (!NumberUtil.acceptableAsInt(pid)) {
                                        Main.getInstance().getLayout().sendString(s, "error.not-a-number", pid);
                                        return;
                                    }
                                    int id = Integer.parseInt(pid);
                                    if (Main.getInstance().getReportManager().isReason(id)) {
                                        Main.getInstance().getLayout().sendString(s, "admin.punish.error.already", pid);
                                        return;
                                    }
                                    String name = ChatColor.translateAlternateColorCodes('&', args[3].replace("_", " "));

                                    Punishment.Type type;
                                    try {
                                        type = Punishment.Type.valueOf(args[4].toUpperCase());
                                    } catch (IllegalArgumentException e) {
                                        Main.getInstance().getLayout().sendString(s, "punishments.error.type-not-found", args[4]);
                                        return;
                                    }

                                    if (!NumberUtil.acceptableAsLong(args[5])) {
                                        Main.getInstance().getLayout().sendString(s, "error.not-a-number", args[5]);
                                        return;
                                    }
                                    long duration = Long.parseLong(args[5]);
                                    if (duration <= 0 && duration != -1) {
                                        Main.getInstance().getLayout().sendString(s, "admin.punish.error.duration-invalid");
                                        return;
                                    }

                                    Punishment pun = new Punishment(id, name, type, duration > 0 ? duration * 1000 : -1);
                                    Main.getInstance().getPunishmentManager().addPunishment(pun);
                                    Main.getInstance().getLayout().sendString(s, "admin.punish.add", pid, pun.getName());
                                    return;
                                }
                                break;
                            case "del":
                                if (args.length > 2) {
                                    String pid = args[2];
                                    if (!NumberUtil.acceptableAsInt(pid)) {
                                        Main.getInstance().getLayout().sendString(s, "error.not-a-number", pid);
                                        return;
                                    }
                                    int id = Integer.parseInt(pid);
                                    Punishment pun = Main.getInstance().getPunishmentManager().getPunishment(id);
                                    if (pun == null) {
                                        Main.getInstance().getLayout().sendString(s, "punishments.error.not-found", pid);
                                        return;
                                    }
                                    Main.getInstance().getPunishmentManager().removePunishment(pun);
                                    Main.getInstance().getLayout().sendString(s, "admin.punish.del", pid, pun.getName());
                                    return;
                                }
                                break;
                        }
                    }
                    break;
            }
        }
        Main.getInstance().getLayout().sendString(s, "admin.help");
    }
}
