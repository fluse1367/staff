package eu.software4you.minecraft.staff.bungee.command;

import eu.software4you.minecraft.staff.bungee.Main;
import eu.software4you.minecraft.staff.bungee.punishment.PlayerPunishment;
import eu.software4you.minecraft.staff.util.FriendlyTime;
import eu.software4you.proxy.player.PermissionsDummyProxiedPlayer;
import eu.software4you.utils.BungeeUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

public class CheckCommand extends Command {
    public CheckCommand() {
        super("check", Main.getInstance().getConfig().string("permission.punish.check.base"));
    }

    @Override
    public void execute(CommandSender s, String[] a) {
        if (a.length > 0) {
            UUID uuid = BungeeUtils.getMainUserCache().getUUID(a[0]);
            if (uuid == null) {
                Main.getInstance().getLayout().sendString(s, "error.player.no-player", a[0]);
                return;
            }
            if (new PermissionsDummyProxiedPlayer(uuid).hasPermission(Main.getInstance().getConfig().string("permission.punish.check.exempt"))) {
                Main.getInstance().getLayout().sendString(s, "error.player.exempt", a[0]);
                return;
            }
            String name = BungeeUtils.getMainUserCache().getUsername(uuid);
            ProxiedPlayer t;
            if ((t = Main.getInstance().getProxy().getPlayer(uuid)) != null) {
                Main.getInstance().getLayout().sendString(s, "check.header.online", name, uuid.toString(), t.getAddress().getAddress().getHostAddress());
            } else {
                Main.getInstance().getLayout().sendString(s, "check.header.offline", name, uuid.toString());
            }

            Main.getInstance().getLayout().sendString(s, "check.active");
            Main.getInstance().getPunishmentManager().getActivePunishments(uuid).forEach(p -> sendPunishmentEntry(s, p));


            if (a.length > 1 && a[1].equalsIgnoreCase("-h")) {
                Main.getInstance().getLayout().sendString(s, "check.history");
                Main.getInstance().getPunishmentManager().collectFormerPunishments(uuid).forEach(p -> sendPunishmentEntry(s, p));
            }
            return;
        }
        Main.getInstance().getLayout().sendString(s, "check.usage");
    }

    private void sendPunishmentEntry(CommandSender receiver, PlayerPunishment pun) {
        Main.getInstance().getLayout().sendString(receiver, "check.punishments",
                pun.getId().toString(),
                String.valueOf(pun.getPunishment().getId()),
                pun.getPunishment().getName(),
                pun.getPunishment().getType().name(),
                pun.getExpiresFriendly(),
                FriendlyTime.time(pun.getTimestamp()),
                BungeeUtils.getMainUserCache().getUsername(pun.getPunisher())
        );
    }
}
