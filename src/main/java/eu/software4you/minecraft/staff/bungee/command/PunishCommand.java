package eu.software4you.minecraft.staff.bungee.command;

import eu.software4you.minecraft.staff.bungee.Main;
import eu.software4you.minecraft.staff.bungee.punishment.PlayerPunishment;
import eu.software4you.minecraft.staff.bungee.punishment.Punishment;
import eu.software4you.minecraft.staff.bungee.punishment.PunishmentManager;
import eu.software4you.minecraft.staff.util.NumberUtil;
import eu.software4you.proxy.configuration.SimpleLayoutWrapper;
import eu.software4you.proxy.player.PermissionsDummyProxiedPlayer;
import eu.software4you.utils.BungeeUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

public class PunishCommand extends Command {
    public PunishCommand() {
        super("punish", Main.getInstance().getConfig().string("permission.punish.base"));
    }

    public static void listPunishments(CommandSender s) {
        SimpleLayoutWrapper wr = new SimpleLayoutWrapper(Main.getInstance().getLayout().section("punishments.list"));
        wr.sendString(s, "header");
        Main.getInstance().getPunishmentManager().getPunishments().forEach(pun -> {
            wr.sendString(s, "body", String.valueOf(pun.getId()), pun.getName(), pun.getType().name(), pun.getDurationFriendly());
        });
        wr.sendString(s, "footer");
    }

    @Override
    public void execute(CommandSender s, String[] a) {
        if (a.length == 2) {
            UUID uuid = BungeeUtils.getMainUserCache().getUUID(a[0]);
            if (uuid == null) {
                Main.getInstance().getLayout().sendString(s, "error.player.no-player", a[0]);
                return;
            }
            if (new PermissionsDummyProxiedPlayer(uuid).hasPermission(Main.getInstance().getConfig().string("permission.punish.exempt"))) {
                Main.getInstance().getLayout().sendString(s, "error.player.exempt", a[0]);
                return;
            }

            String punishId = a[1];
            PunishmentManager man = Main.getInstance().getPunishmentManager();
            int pid;
            if (!NumberUtil.acceptableAsInt(punishId) || !man.isPunishment(pid = Integer.parseInt(punishId))) {
                Main.getInstance().getLayout().sendString(s, "punishments.error.not-found", punishId);
                return;
            }
            Punishment pun = man.getPunishment(pid);

            String perm = Main.getInstance().getConfig().string("permission.punish.specific", String.valueOf(pun.getId()));
            if (!s.hasPermission(perm)) {
                Main.getInstance().getLayout().sendString(s, "punishments.error.no-permission", String.valueOf(pun.getId()), pun.getName());
                return;
            }

            PlayerPunishment ppun = man.getActivePunish(uuid, pun.getType());

            if (ppun != null && ppun.isActive()) {
                ppun.updatePunishment(pun);
            } else {
                UUID punisher = s instanceof ProxiedPlayer ? ((ProxiedPlayer) s).getUniqueId() : Main.CONSOLE_UUID;
                ppun = man.punishPlayer(uuid, punisher, pun);
            }

            Main.getInstance().getLayout().sendString(s, "punishments.punished",
                    BungeeUtils.getMainUserCache().getUsername(uuid), s.getName(), ppun.getId().toString(), String.valueOf(pun.getId()),
                    pun.getName(), pun.getType().name().toLowerCase(), pun.getDurationFriendly());

            return;
        }
        listPunishments(s);
        Main.getInstance().getLayout().sendString(s, "punishments.usage");
    }
}
