package eu.software4you.minecraft.staff.bungee.report;

public class ReportReason {
    private final int id;
    private final String name;

    public ReportReason(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
