package eu.software4you.minecraft.staff.bungee.report;

import eu.software4you.common.validation.Initiable;
import eu.software4you.common.validation.Initiatable;
import eu.software4you.minecraft.staff.bungee.Main;
import eu.software4you.proxy.psb.PSB;
import eu.software4you.utils.BungeeUtils;
import eu.software4you.utils.StringUtils;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.UUID;

public class ReportSession extends Initiable {
    private final String id;
    private final UUID reporter;
    private final UUID reported;
    private final ReportReason reason;

    private UUID supporter;

    private ReportSession(String id, UUID reporter, UUID reported, ReportReason reason) {
        this.id = id;
        this.reporter = reporter;
        this.reported = reported;
        this.reason = reason;
    }

    public static ReportSession newSession(UUID reporter, UUID reported, ReportReason reason) {
        return new ReportSession(StringUtils.randomString(5), reporter, reported, reason);
    }

    public String getId() {
        return id;
    }

    public UUID getReporter() {
        return reporter;
    }

    public UUID getReported() {
        return reported;
    }

    public UUID getSupporter() {
        return supporter;
    }

    public ReportReason getReason() {
        return reason;
    }

    public boolean isSupporting() {
        return supporter != null;
    }

    public void maintainBy(ProxiedPlayer supporter) {
        initiate();
        this.supporter = supporter.getUniqueId();
        Main.getInstance().getReportManager().removeSession(this);

        ProxiedPlayer t = ProxyServer.getInstance().getPlayer(reported);
        if (t == null) {
            Main.getInstance().getLayout().sendString(supporter, "error.player.not-online", BungeeUtils.getMainUserCache().getUsername(reported));
            return;
        }

        PSB.setTargetServer(t.getServer().getInfo());

        Main.getInstance().async(() -> {
            PSB.request(String.format("queuevanish %s", supporter.getUniqueId().toString()));
        });
        Main.getInstance().async(() -> {
            PSB.request(String.format("queueteleport %s %s", supporter.getUniqueId().toString(), reported.toString()));
        });

        String[] repl = {
                id,
                BungeeUtils.getMainUserCache().getUsername(reporter),
                BungeeUtils.getMainUserCache().getUsername(reported),
                String.valueOf(reason.getId()),
                reason.getName()
        };

        Main.getInstance().queueBroadcastStaffChat(Main.getInstance().getLayout().string("reportaccept.broadcast",
                supporter.getDisplayName(), repl), supporter);

        Main.getInstance().getLayout().sendString(supporter, "reportaccept.message", repl);

        supporter.connect(t.getServer().getInfo());

    }
}
