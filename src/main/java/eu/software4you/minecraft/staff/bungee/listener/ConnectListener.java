package eu.software4you.minecraft.staff.bungee.listener;


import eu.software4you.minecraft.staff.bungee.Main;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.HashMap;

public class ConnectListener implements Listener {
    private final HashMap<ProxiedPlayer, ServerInfo> previousServers = new HashMap<>();

    @EventHandler
    public void on(ServerConnectedEvent e) {
        ProxiedPlayer p = e.getPlayer();
        if (!p.hasPermission(Main.getInstance().getConfig().string("permission.chat.send")))
            return;
        if (previousServers.containsKey(p))
            Main.getInstance().queueBroadcastStaffChat(Main.getInstance().getLayout().string("connections.switch",
                    p.getDisplayName(), e.getServer().getInfo().getName(), previousServers.get(p).getName()));
        else
            Main.getInstance().queueBroadcastStaffChat(Main.getInstance().getLayout().string("connections.connect",
                    p.getDisplayName(), e.getServer().getInfo().getName()));
        previousServers.remove(p);
    }

    @EventHandler(priority = 127)
    public void on(ServerConnectEvent e) {
        if (e.isCancelled())
            return;
        if (!e.getPlayer().hasPermission(Main.getInstance().getConfig().string("permission.chat.send")))
            return;
        ProxiedPlayer p = e.getPlayer();
        if (p.getServer() == null)
            return;
        previousServers.put(p, p.getServer().getInfo());
    }

    @EventHandler
    public void on(PlayerDisconnectEvent e) {
        ProxiedPlayer p = e.getPlayer();
        previousServers.remove(p);
        if (!p.hasPermission(Main.getInstance().getConfig().string("permission.chat.send")))
            return;
        Main.getInstance().queueBroadcastStaffChat(Main.getInstance().getLayout().string("connections.disconnect",
                p.getDisplayName(), p.getServer().getInfo().getName()));
    }
}
