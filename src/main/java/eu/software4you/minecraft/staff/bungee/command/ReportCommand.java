package eu.software4you.minecraft.staff.bungee.command;

import eu.software4you.minecraft.staff.bungee.Main;
import eu.software4you.minecraft.staff.bungee.report.ReportManager;
import eu.software4you.minecraft.staff.bungee.report.ReportReason;
import eu.software4you.minecraft.staff.bungee.report.ReportSession;
import eu.software4you.minecraft.staff.util.NumberUtil;
import eu.software4you.proxy.configuration.SimpleLayoutWrapper;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class ReportCommand extends Command {
    public ReportCommand() {
        super("report");
    }

    public static void listReasons(CommandSender s) {
        SimpleLayoutWrapper wr = new SimpleLayoutWrapper(Main.getInstance().getLayout().section("report.reasons.list"));
        wr.sendString(s, "header");
        Main.getInstance().getReportManager().getReasons().forEach(reason -> {
            wr.sendString(s, "body", String.valueOf(reason.getId()), reason.getName());
        });
        wr.sendString(s, "footer");
    }

    @Override
    public void execute(CommandSender s, String[] a) {
        if (!(s instanceof ProxiedPlayer))
            return;
        ProxiedPlayer p = (ProxiedPlayer) s;
        if (a.length == 2) {
            ProxiedPlayer target = ProxyServer.getInstance().getPlayer(a[0]);
            if (target == null) {
                Main.getInstance().getLayout().sendString(p, "error.player.not-online", a[0]);
                return;
            }
            if (target.hasPermission(Main.getInstance().getConfig().string("permission.report.exempt"))) {
                Main.getInstance().getLayout().sendString(p, "error.player.exempt", a[0]);
                return;
            }


            String reasonId = a[1];
            ReportManager man = Main.getInstance().getReportManager();
            int rid;
            if (!NumberUtil.acceptableAsInt(reasonId) || !man.isReason(rid = Integer.parseInt(reasonId))) {
                Main.getInstance().getLayout().sendString(p, "report.reasons.error.not-found", reasonId);
                return;
            }
            ReportReason reason = man.getReason(rid);

            ReportSession report = ReportSession.newSession(p.getUniqueId(), target.getUniqueId(), reason);

            man.addSession(report);

            Main.getInstance().getLayout().sendString(p, "report.reported",
                    target.getDisplayName(), report.getId(), reasonId, report.getReason().getName());

            TextComponent msg = new TextComponent(Main.getInstance().getLayout().string("report.staff.header",
                    p.getDisplayName(), target.getDisplayName(), reasonId, report.getReason().getName(),
                    report.getId(), target.getServer().getInfo().getName()));

            TextComponent accept = new TextComponent(Main.getInstance().getLayout().string("report.staff.accept.text"));
            accept.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{
                    new TextComponent(Main.getInstance().getLayout().string("report.staff.accept.hover"))}));
            accept.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/reportaccept " + report.getId()));

            Main.getInstance().async(() -> {
                Main.getInstance().broadcastStaffChat(msg);
                Main.getInstance().broadcastStaffChat(accept);
            });
            return;
        }
        listReasons(p);
        Main.getInstance().getLayout().sendString(p, "report.usage");
    }
}
