package eu.software4you.minecraft.staff.bungee.command;

import eu.software4you.minecraft.staff.bungee.Main;
import eu.software4you.minecraft.staff.bungee.report.ReportSession;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class ReportAcceptCommand extends Command {
    public ReportAcceptCommand() {
        super("reportaccept", Main.getInstance().getConfig().string("permission.report.accept"));
    }

    @Override
    public void execute(CommandSender s, String[] a) {
        if (!(s instanceof ProxiedPlayer))
            return;
        ProxiedPlayer p = (ProxiedPlayer) s;
        if (a.length != 1) {
            Main.getInstance().getLayout().sendString(p, "reportaccept.error.usage");
            return;
        }
        String id = a[0];

        ReportSession report = Main.getInstance().getReportManager().getSession(id);
        if (report == null) {
            Main.getInstance().getLayout().sendString(p, "reportaccept.error.id", id);
            return;
        }

        report.maintainBy(p);
    }
}
