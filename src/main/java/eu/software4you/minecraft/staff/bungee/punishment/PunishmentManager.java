package eu.software4you.minecraft.staff.bungee.punishment;

import eu.software4you.sql.CachedSqlTableWrapper;
import eu.software4you.sql.SqlEngine;

import java.sql.SQLException;
import java.util.*;

public class PunishmentManager {
    private final CachedSqlTableWrapper<Integer> punishmentWrapper;
    private final Map<Integer, Punishment> punishments = new HashMap<>();
    private final PunishmentHandler handler;

    public PunishmentManager(SqlEngine engine, PunishmentHandler handler) {
        this.handler = handler;
        this.punishmentWrapper = new CachedSqlTableWrapper<>(engine, engine.getDefaultTable("punishments"), "id");
        punishmentWrapper.cache();
        punishmentWrapper.getCache().forEach((id, map) -> {
            punishments.put(id, new Punishment(id, String.valueOf(map.get("name")),
                    Punishment.Type.valueOf(String.valueOf(map.get("type"))),
                    Long.parseLong(String.valueOf(map.get("duration")))));
        });
        punishmentWrapper.purgeCache();
    }

    public boolean isPunishment(int id) {
        return getPunishment(id) != null;
    }

    public Punishment getPunishment(int id) {
        return punishments.get(id);
    }

    public void addPunishment(Punishment punishment) {
        int id = punishment.getId();
        if (getPunishment(id) != null)
            throw new IllegalArgumentException("Punishment id already exists");
        try {
            punishmentWrapper.getSqlEngine().getDefaultTable("punishments").insertValues(
                    new Object[]{id, punishment.getName(), punishment.getType().name(), punishment.getDuration()});
        } catch (SQLException e) {
            throw new Error(e);
        }
        punishments.put(id, punishment);
    }

    public void removePunishment(Punishment punishment) {
        int id = punishment.getId();
        if (getPunishment(id) == null)
            throw new IllegalArgumentException("Punishment id does not exist");
        punishmentWrapper.delete(id);
        punishments.remove(id);
    }

    public Collection<Punishment> getPunishments() {
        List<Punishment> li = new ArrayList<>(punishments.values());
        li.sort(Comparator.comparingInt(Punishment::getId));
        return Collections.unmodifiableList(li);
    }

    public PlayerPunishment punishPlayer(UUID player, UUID punisher, Punishment punishment) {
        PlayerPunishment pp = new PlayerPunishment(punishmentWrapper.getSqlEngine(), handler, UUID.randomUUID(),
                player, punisher, punishment, System.currentTimeMillis(),
                punishment.getDuration() > 0 ? System.currentTimeMillis() + punishment.getDuration() : -1);
        pp.sqlInsert();
        handler.applyPunishment(pp);
        return pp;
    }

    public PlayerPunishment getActivePunish(UUID player, Punishment.Type type) {
        return handler.getActivePunish(player, type);
    }

    public Collection<PlayerPunishment> getActivePunishments(UUID player) {
        return handler.getActivePunishments(player);
    }

    public Collection<PlayerPunishment> collectFormerPunishments(UUID player) {
        return handler.collectFormerPunishments(player);
    }

    public boolean hasActivePunish(UUID player, Punishment.Type type) {
        return handler.hasActivePunish(player, type);
    }
}
