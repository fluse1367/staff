package eu.software4you.minecraft.staff.bungee;

import eu.software4you.configuration.SimpleConfigurationWrapper;
import eu.software4you.minecraft.staff.bungee.command.*;
import eu.software4you.minecraft.staff.bungee.listener.ConnectListener;
import eu.software4you.minecraft.staff.bungee.listener.StaffChatListener;
import eu.software4you.minecraft.staff.bungee.listener.WelcomeListener;
import eu.software4you.minecraft.staff.bungee.punishment.PunishmentHandler;
import eu.software4you.minecraft.staff.bungee.punishment.PunishmentManager;
import eu.software4you.minecraft.staff.bungee.report.ReportManager;
import eu.software4you.minecraft.staff.util.FriendlyTime;
import eu.software4you.proxy.s4yplugin.S4YProxyPluginExt;
import eu.software4you.sql.SqlEngine;
import eu.software4you.utils.BungeeUtils;
import eu.software4you.utils.license.DefaultLicensable;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;


public class Main extends S4YProxyPluginExt implements DefaultLicensable {
    public static final UUID CONSOLE_UUID = UUID.fromString("78be592b-97ca-4d98-b35b-c7687cc6af9a");
    public static final String CONSOLE_NAME = "§cCONSOLE";
    private static Main instance;

    private final List<UUID> optOut = new ArrayList<>();
    private SqlEngine sql;
    private ReportManager reportManager;
    private PunishmentManager punishmentManager;

    public static Main getInstance() {
        return instance;
    }

    public ReportManager getReportManager() {
        return reportManager;
    }

    public PunishmentManager getPunishmentManager() {
        return punishmentManager;
    }

    public boolean isReceivingStaffNotifications(UUID uuid) {
        return !optOut.contains(uuid);
    }

    public void setReceivingStaffNotifications(UUID uuid, boolean receives) {
        if (receives)
            optOut.remove(uuid);
        else if (!optOut.contains(uuid))
            optOut.add(uuid);
    }

    @Override
    public void enable() throws Exception {
        instance = this;

        prepareSql();

        sql.disableAutomaticParameterizedQueries = true;
        sql.connect(true);

        if (!BungeeUtils.isMainUserCacheEnabled())
            BungeeUtils.enableMainUserCache();
        BungeeUtils.getMainUserCache().cache(CONSOLE_UUID, CONSOLE_NAME);

        FriendlyTime.setLayout(getLayout().sub("time-formats"));

        reportManager = new ReportManager(sql);
        PunishmentHandler punishmentHandler = new PunishmentHandler(sql);
        registerEvents(punishmentHandler);
        punishmentManager = new PunishmentManager(sql, punishmentHandler);

        registerEvents(new StaffChatListener());
        registerEvents(new WelcomeListener());
        registerEvents(new ConnectListener());

        registerCommand(new ToggleCommand());
        registerCommand(new ReportCommand());
        registerCommand(new ReportAcceptCommand());
        registerCommand(new StaffAdminCommand());
        registerCommand(new PunishCommand());
        registerCommand(new PunishRevokeCommand());
        registerCommand(new CheckCommand());

    }

    private void prepareSql() throws Exception {
        if (sql != null)
            throw new IllegalAccessException("Sql already initialized!");

        SimpleConfigurationWrapper backend = getConfig().sub("sql.backends." + getConfig().string("sql.backend"));
        SqlEngine.ConnectionData cdata;
        String type;
        if ((type = backend.string("type", (Object) "file")).equalsIgnoreCase("file")) {
            cdata = new SqlEngine.ConnectionData(new File(getDataFolder(), backend.string("file", (Object) "storage.db")));
        } else if (type.equalsIgnoreCase("mysql")) {
            cdata = new SqlEngine.ConnectionData(
                    backend.string("host", (Object) "localhost"),
                    backend.section().getInt("port", 3306),
                    backend.string("user"),
                    backend.string("password"),
                    backend.string("database")
            );
        } else {
            throw new IllegalArgumentException("SQL-Backend-Type " + type + " not acceptable. Only acceptable types: file, mysql");
        }

        sql = new SqlEngine(cdata);

        SqlEngine.Table table = sql.newTable("report_reasons");
        table.addDefaultKey(new SqlEngine.Key("id", SqlEngine.Key.KeyType.Integer).notNull().primary());
        table.addDefaultKey(new SqlEngine.Key("name", SqlEngine.Key.KeyType.String).notNull());
        sql.addDefaultTable(table);

        table = sql.newTable("punishments");
        table.addDefaultKey(new SqlEngine.Key("id", SqlEngine.Key.KeyType.Integer).notNull().primary());
        table.addDefaultKey(new SqlEngine.Key("name", SqlEngine.Key.KeyType.String).notNull());
        table.addDefaultKey(new SqlEngine.Key("type", SqlEngine.Key.KeyType.String).notNull());
        table.addDefaultKey(new SqlEngine.Key("duration", SqlEngine.Key.KeyType.BigInteger).notNull());
        sql.addDefaultTable(table);

        table = sql.newTable("player_punishments");
        table.addDefaultKey(new SqlEngine.Key("id", SqlEngine.Key.KeyType.VariableCharacter).notNull().size(36).primary());
        table.addDefaultKey(new SqlEngine.Key("player", SqlEngine.Key.KeyType.VariableCharacter).notNull().size(36));
        table.addDefaultKey(new SqlEngine.Key("punisher", SqlEngine.Key.KeyType.VariableCharacter).notNull().size(36));
        table.addDefaultKey(new SqlEngine.Key("punishment", SqlEngine.Key.KeyType.Integer).notNull());
        table.addDefaultKey(new SqlEngine.Key("timestamp", SqlEngine.Key.KeyType.BigInteger).notNull());
        table.addDefaultKey(new SqlEngine.Key("expires", SqlEngine.Key.KeyType.BigInteger).notNull());
        sql.addDefaultTable(table);


    }

    @Override
    public void disable() throws Exception {
        if (sql != null && sql.isConnected())
            sql.disconnect(true);
    }

    @Override
    public String id() {
        return "jgj8ym2aludqx846mqcgid";
    }

    @Override
    public String getDefaultLicenseKey() {
        return "P1L9-BTUV-E8O2-0ZJF";
    }

    public void queueBroadcastStaffChat(ProxiedPlayer p, String msg) {
        queueBroadcastStaffChat(getLayout().string("received.normal",
                p.getServer().getInfo().getName(),
                p.getDisplayName(),
                msg,
                ChatColor.translateAlternateColorCodes('&', msg)));
    }

    public void queueBroadcastAdminChat(ProxiedPlayer p, String msg) {
        queueBroadcastAdminChat(getLayout().string("received.admin",
                p.getServer().getInfo().getName(),
                p.getDisplayName(),
                msg,
                ChatColor.translateAlternateColorCodes('&', msg)));
    }

    public void queueBroadcastStaffChat(String msg, ProxiedPlayer... exempt) {
        async(() -> broadcastStaffChat(msg, exempt));
    }

    public void queueBroadcastAdminChat(String msg, ProxiedPlayer... exempt) {
        async(() -> broadcastAdminChat(msg, exempt));
    }

    public void broadcastStaffChat(String msg, ProxiedPlayer... exempt) {
        broadcastStaffChat(new TextComponent(msg), exempt);
    }

    public void broadcastAdminChat(String msg, ProxiedPlayer... exempt) {
        broadcastAdminChat(new TextComponent(msg), exempt);
    }

    public void queueBroadcastStaffChat(TextComponent comp, ProxiedPlayer... exempt) {
        async(() -> broadcastStaffChat(comp, exempt));
    }

    public void queueBroadcastAdminChat(TextComponent comp, ProxiedPlayer... exempt) {
        async(() -> broadcastAdminChat(comp, exempt));
    }

    public void broadcastStaffChat(TextComponent comp, ProxiedPlayer... exempt) {
        broadcastChat(comp, "chat.receive", exempt);
    }

    public void broadcastAdminChat(TextComponent comp, ProxiedPlayer... exempt) {
        broadcastChat(comp, "admin-chat.receive", exempt);
    }

    public void broadcastChat(TextComponent comp, String permissionPath, ProxiedPlayer... exempt) {
        List<ProxiedPlayer> exempts = Arrays.asList(exempt);
        getProxy().getPlayers().forEach(all -> {
            if (!exempts.contains(all)
                    && all.hasPermission(getConfig().string("permission." + permissionPath))
                    && isReceivingStaffNotifications(all.getUniqueId())) {
                all.sendMessage(comp);
            }
        });
    }
}
