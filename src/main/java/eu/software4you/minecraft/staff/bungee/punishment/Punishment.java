package eu.software4you.minecraft.staff.bungee.punishment;

import eu.software4you.minecraft.staff.util.FriendlyTime;

public class Punishment {
    private final int id;
    private final String name;
    private final Type type;
    private final long duration;

    public Punishment(int id, String name, Type type, long duration) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.duration = duration;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    public long getDuration() {
        return duration;
    }

    public String getDurationFriendly() {
        return FriendlyTime.duration(duration);
    }

    public enum Type {
        BAN, MUTE
    }
}
