package eu.software4you.minecraft.staff.bungee.punishment;

import eu.software4you.minecraft.staff.bungee.Main;
import eu.software4you.minecraft.staff.util.FriendlyTime;
import eu.software4you.sql.CachedSqlTableEntryWrapper;
import eu.software4you.sql.SqlEngine;
import eu.software4you.sql.SqlTableEntryWrapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class PlayerPunishment {
    private final CachedSqlTableEntryWrapper<String> wrapper;
    private final PunishmentHandler handler;
    private final UUID id;
    private final UUID player;
    private final UUID punisher;
    private final long timestamp;
    private long expires;
    private Punishment punishment;

    PlayerPunishment(SqlEngine engine, PunishmentHandler handler, UUID id, UUID player, UUID punisher, Punishment punishment, long timestamp, long expires) {
        this.handler = handler;
        this.id = id;
        this.player = player;
        this.punisher = punisher;
        this.punishment = punishment;
        this.timestamp = timestamp;
        this.expires = expires;

        wrapper = new CachedSqlTableEntryWrapper<>(engine, engine.getDefaultTable("player_punishments"), "id", id.toString());
    }

    static PlayerPunishment fromResultSet(SqlEngine engine, PunishmentHandler handler, ResultSet res) {
        try {
            return new PlayerPunishment(engine,
                    handler,
                    UUID.fromString(res.getString("id")),
                    UUID.fromString(res.getString("player")),
                    UUID.fromString(res.getString("punisher")),
                    Main.getInstance().getPunishmentManager().getPunishment(res.getInt("punishment")),
                    res.getLong("timestamp"),
                    res.getLong("expires")
            );
        } catch (SQLException e) {
            throw new Error(e);
        }
    }

    public UUID getId() {
        return id;
    }

    public UUID getPlayer() {
        return player;
    }

    public UUID getPunisher() {
        return punisher;
    }

    public Punishment getPunishment() {
        return punishment;
    }

    public void updatePunishment(Punishment punishment) {
        updatePunishment(punishment, punishment.getDuration() > 0 ? timestamp + punishment.getDuration() : -1);
    }

    public void updatePunishment(Punishment punishment, long expires) {
        this.punishment = punishment;
        updateExpires(expires);
        if (wrapper.exists()) {
            wrapper.update("punishment", punishment.getId());
        }
        handler.punishActionPlayer(this);
    }

    public void updateExpires(long expires) {
        this.expires = expires;
        if (wrapper.exists())
            wrapper.update("expires", expires);
    }

    public long getTimestamp() {
        return timestamp;
    }

    public long getExpires() {
        return expires;
    }

    public String getExpiresFriendly() {
        return FriendlyTime.time(expires);
    }

    public long getMillisLeft() {
        return expires > 0 ? Math.max(0, expires - System.currentTimeMillis()) : -1;
    }

    public boolean isActive() {
        return expires < 0 || expires > 0 && getMillisLeft() > 0;
    }

    public void delete() {
        if (wrapper.exists())
            wrapper.delete();
        handler.deletePunishment(this);
    }

    SqlTableEntryWrapper<String> sqlTableEntryWrapper() {
        return wrapper;
    }

    void sqlInsert() {
        try {
            wrapper.getTable().insertValues(id.toString(), player.toString(), punisher.toString(), punishment.getId(), timestamp, expires);
        } catch (SQLException e) {
            throw new Error(e);
        }
    }

}
