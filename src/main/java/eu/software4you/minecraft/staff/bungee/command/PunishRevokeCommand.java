package eu.software4you.minecraft.staff.bungee.command;

import eu.software4you.minecraft.staff.bungee.Main;
import eu.software4you.minecraft.staff.bungee.punishment.PlayerPunishment;
import eu.software4you.minecraft.staff.bungee.punishment.Punishment;
import eu.software4you.utils.BungeeUtils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

public class PunishRevokeCommand extends Command {
    public PunishRevokeCommand() {
        super("punishrevoke", Main.getInstance().getConfig().string("permission.punish.revoke.base"));
    }

    @Override
    public void execute(CommandSender s, String[] a) {
        if (a.length > 1) {
            UUID uuid = BungeeUtils.getMainUserCache().getUUID(a[0]);
            if (uuid == null) {
                Main.getInstance().getLayout().sendString(s, "error.player.not-player", a[0]);
                return;
            }
            String name = BungeeUtils.getMainUserCache().getUsername(uuid);

            Punishment.Type type;
            try {
                type = Punishment.Type.valueOf(a[1].toUpperCase());
            } catch (IllegalArgumentException e) {
                Main.getInstance().getLayout().sendString(s, "punishments.error.type-not-found", a[1]);
                return;
            }

            PlayerPunishment ppun = Main.getInstance().getPunishmentManager().getActivePunish(uuid, type);
            if (ppun == null) {
                Main.getInstance().getLayout().sendString(s, "punishrevoke.error.no-punishment", type.name().toLowerCase(), name);
                return;
            }

            Punishment pun = ppun.getPunishment();
            String perm = Main.getInstance().getConfig().string("permission.punish.revoke.specific", String.valueOf(pun.getId()));
            if (!s.hasPermission(perm)) {
                Main.getInstance().getLayout().sendString(s, "punishrevoke.error.no-permission", String.valueOf(pun.getId()), pun.getName());
                return;
            }
            ppun.updateExpires(System.currentTimeMillis());
            Main.getInstance().getLayout().sendString(s, "punishrevoke.revoked", type.name().toLowerCase(), name);
            return;
        }
        Main.getInstance().getLayout().sendString(s, "punishrevoke.usage");
    }
}
