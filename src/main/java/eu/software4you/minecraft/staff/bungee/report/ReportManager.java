package eu.software4you.minecraft.staff.bungee.report;

import eu.software4you.sql.CachedSqlTableWrapper;
import eu.software4you.sql.SqlEngine;

import java.sql.SQLException;
import java.util.*;

public class ReportManager {
    private final CachedSqlTableWrapper<Integer> reasons;
    private final Map<Integer, ReportReason> reportReasons = new HashMap<>();
    private final Map<String, ReportSession> reportSessions = new HashMap<>();

    public ReportManager(SqlEngine engine) {
        reasons = new CachedSqlTableWrapper<>(engine, engine.getDefaultTable("report_reasons"), "id");
        reasons.cache();
        reasons.getCache().forEach((id, map) -> {
            reportReasons.put(id, new ReportReason(id, (String) map.get("name")));
        });
        reasons.purgeCache();


    }

    public boolean isReason(int id) {
        return getReason(id) != null;
    }

    public ReportReason getReason(int id) {
        return reportReasons.get(id);
    }

    public void addReason(ReportReason reason) {
        int id = reason.getId();
        if (getReason(id) != null)
            throw new IllegalArgumentException("Reason id already exists");
        try {
            reasons.getSqlEngine().getDefaultTable("report_reasons").insertValues(new Object[]{id, reason.getName()});
        } catch (SQLException e) {
            throw new Error(e);
        }
        reportReasons.put(id, reason);
    }

    public void removeReason(ReportReason reason) {
        int id = reason.getId();
        if (getReason(id) == null)
            throw new IllegalArgumentException("Reason id does not exist");
        reasons.delete(id);
        reportReasons.remove(id);
    }

    public Collection<ReportReason> getReasons() {
        List<ReportReason> li = new ArrayList<>(reportReasons.values());
        li.sort(Comparator.comparingInt(ReportReason::getId));
        return Collections.unmodifiableList(li);
    }

    public boolean isSession(String id) {
        return getSession(id) != null;
    }

    public ReportSession getSession(String id) {
        return reportSessions.get(id);
    }

    public void addSession(ReportSession session) {
        if (getSession(session.getId()) != null)
            throw new IllegalArgumentException("Session id already exists");
        reportSessions.put(session.getId(), session);
    }

    public void removeSession(ReportSession session) {
        if (getSession(session.getId()) == null)
            throw new IllegalArgumentException("Session id does not exist");
        reportSessions.remove(session.getId());
    }


}
