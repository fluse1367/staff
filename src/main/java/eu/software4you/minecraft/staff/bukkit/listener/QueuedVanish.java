package eu.software4you.minecraft.staff.bukkit.listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class QueuedVanish implements Listener {
    private final List<UUID> queued = new ArrayList<>();

    public void add(UUID player) {
        Player p = Bukkit.getPlayer(player);
        if (p != null) {
            vanish(p);
            return;
        }
        queued.add(player);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void handle(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        if (queued.contains(p.getUniqueId())) {
            vanish(p);
            e.setJoinMessage(null);
            queued.remove(p.getUniqueId());
        }
    }

    private void vanish(Player p) {
        Bukkit.getOnlinePlayers().forEach(all -> {
            if (p != all)
                all.hidePlayer(p);
        });
    }

}
