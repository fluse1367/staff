package eu.software4you.minecraft.staff.bukkit.listener;

import eu.software4you.minecraft.staff.bukkit.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.HashMap;
import java.util.UUID;

public class QueuedTeleport implements Listener {
    private final HashMap<UUID, UUID> queued = new HashMap<>();

    public void add(UUID player, UUID target) {
        Player p = Bukkit.getPlayer(player);
        Player t = Bukkit.getPlayer(target);
        if (p != null && t != null) {
            Main.getInstance().sync(() -> p.teleport(t), 4);
            return;
        }
        queued.put(player, target);
    }

    @EventHandler
    public void handle(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        if (queued.containsKey(p.getUniqueId())) {
            Player t = Bukkit.getPlayer(queued.get(p.getUniqueId()));
            if (t != null)
                Main.getInstance().sync(() -> p.teleport(t), 4);
            queued.remove(p.getUniqueId());
        }
    }

}
