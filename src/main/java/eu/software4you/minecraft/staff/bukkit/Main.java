package eu.software4you.minecraft.staff.bukkit;

import eu.software4you.minecraft.s4yplugin.S4YJavaPlugin;
import eu.software4you.minecraft.spigot.psb.PSB;
import eu.software4you.minecraft.spigot.psb.command.Command;
import eu.software4you.minecraft.staff.bukkit.listener.QueuedTeleport;
import eu.software4you.minecraft.staff.bukkit.listener.QueuedVanish;
import eu.software4you.utils.license.Licensable;

import java.util.UUID;

public class Main extends S4YJavaPlugin implements Licensable {
    private static Main instance;
    private QueuedTeleport teleportQueue;
    private QueuedVanish vanishQueue;

    public static Main getInstance() {
        return instance;
    }

    @Override
    public void enable() throws Exception {
        instance = this;
        // queue teleport

        registerEvents(teleportQueue = new QueuedTeleport());
        registerEvents(vanishQueue = new QueuedVanish());
        Command c = new Command("queueteleport");
        c.setExecutor(a -> {
            if (a.length >= 2) {
                try {
                    teleportQueue.add(UUID.fromString(a[0]), UUID.fromString(a[1]));
                } catch (IllegalArgumentException ignored) {

                }
            }
            return null;
        });
        PSB.registerCommand(c);
        c = new Command("queuevanish");
        c.setExecutor(a -> {
            if (a.length >= 1) {
                try {
                    vanishQueue.add(UUID.fromString(a[0]));
                } catch (IllegalArgumentException ignored) {

                }
            }
            return null;
        });
        PSB.registerCommand(c);
    }

    @Override
    public void disable() throws Exception {

    }

    @Override
    public String id() {
        return "jgj8ym2aludqx846mqcgid";
    }
}
